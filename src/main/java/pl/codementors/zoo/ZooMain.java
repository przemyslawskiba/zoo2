package pl.codementors.zoo;

import pl.codementors.zoo.animals.Animal;
import pl.codementors.zoo.animals.Bird;
import pl.codementors.zoo.animals.Lizard;
import pl.codementors.zoo.animals.Mammal;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

/**
 * Application main class.
 *
 * @author psysiu
 */
public class ZooMain {

    /**
     * Application starting method.
     *
     * @param args Application starting params.
     */
    public static void main(String[] args) {

        List<Animal> animals = new ArrayList<>();

        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();

                switch (type) {
                    case "Bird": {
                        animals.add(new Bird(name, color));
                        break;
                    }
                    case "Lizard": {
                        animals.add(new Lizard(name, color));
                        break;
                    }
                    case "Mammal": {
                        animals.add(new Mammal(name, color));
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }

        for (Animal animal : animals) {
            System.out.println(animal);
        }

    }

}
