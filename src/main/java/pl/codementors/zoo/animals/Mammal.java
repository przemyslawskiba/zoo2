package pl.codementors.zoo.animals;

/**
 * Just a mammal with some fur.
 *
 * @author psysiu
 */
public class Mammal extends Animal {

    /**
     * Color of the animal fur.
     */
    private String furColor;

    public Mammal(String name, String furColor) {
        super(name);
        this.furColor = furColor;
    }

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
