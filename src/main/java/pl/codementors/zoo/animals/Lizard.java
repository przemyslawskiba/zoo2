package pl.codementors.zoo.animals;

/**
 * Lizard with some scales.
 *
 * @author psysiu
 */
public class Lizard extends Animal {

    /**
     * Color of the animal scales.
     */
    private String scalesColor;

    public Lizard(String name, String scalesColor) {
        super(name);
        this.scalesColor = scalesColor;
    }

    public String getScalesColor() {
        return scalesColor;
    }

    public void setScalesColor(String scalesColor) {
        this.scalesColor = scalesColor;
    }
}
